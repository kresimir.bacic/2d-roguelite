﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
    public Direction openingDirection;

    /*
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.compareTag("RoomCenter"))
        {

        }
    }*/
    /*
    private RoomTemplates templates;
    private int rand;
    private bool spawned = false;
    private static int roomsRemaining = 4;
    
    private void Start()
    {
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke("Spawn", 0.1f);
    }

    private void Spawn()
    {
        if (!spawned)
        {
            switch (openingDirection)
            {
                case Direction.DIRECTION_UP:
                    //  Spawn a T- room
                    rand = Random.Range(0, templates.topRooms.Length);
                    if (roomsRemaining <= 0) rand = 0;
                    roomsRemaining--;
                    Instantiate(templates.topRooms[rand], transform.position, Quaternion.identity);
                    break;
                case Direction.DIRECTION_DOWN:
                    //  Spawn a B- room
                    rand = Random.Range(0, templates.bottomRooms.Length);
                    if (roomsRemaining <= 0) rand = 0;
                    roomsRemaining--;
                    Instantiate(templates.bottomRooms[rand], transform.position, Quaternion.identity);
                    break;
                case Direction.DIRECTION_LEFT:
                    //  Spawn a L- room
                    rand = Random.Range(0, templates.leftRooms.Length);
                    if (roomsRemaining <= 0) rand = 0;
                    roomsRemaining--;
                    Instantiate(templates.leftRooms[rand], transform.position, Quaternion.identity);
                    break;
                case Direction.DIRECTION_RIGHT:
                    //  Spawn a R- room
                    rand = Random.Range(0, templates.rightRooms.Length);
                    if (roomsRemaining <= 0) rand = 0;
                    roomsRemaining--;
                    Instantiate(templates.rightRooms[rand], transform.position, Quaternion.identity);
                    break;

                default: break;
            }
            spawned = true;
        }
    }*/

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("RoomCenter"))
        {
            Destroy(gameObject);
        }
    }
    
}


public enum Direction
{
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    NO_DOOR_UP,
    NO_DOOR_DOWN,
    NO_DOOR_LEFT,
    NO_DOOR_RIGHT
};
