﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform player = null;
    private bool following = false;

    //  Makes the camera snap to the room player is currently in
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player != null)
        {
            following = true;
        }
        else
        {
            following = false;
        }

        if(following)
        {
            Transform playerTransform = player.GetComponent<Transform>();

            if (playerTransform.position.x > (this.transform.position.x + 2))
            {
                this.transform.Translate(new Vector3(4, 0, 0));
            }
            else if (playerTransform.position.x < (this.transform.position.x - 2))
            {
                this.transform.Translate(new Vector3(-4, 0, 0));
            }

            if (playerTransform.position.y > (this.transform.position.y + 2))
            {
                this.transform.Translate(new Vector3(0, 4, 0));
            }
            else if (playerTransform.position.y < (this.transform.position.y - 2))
            {
                this.transform.Translate(new Vector3(0, -4, 0));
            }
        }
    }
}
