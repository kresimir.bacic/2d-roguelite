﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastAbility : Ability
{
    public LineRenderer lineRenderer;
    public float laserLength;
    public WaitForSeconds shotDuration = new WaitForSeconds(1.0f);
    public int damage;
    public float hitForce;
    public int maxTargets;
    public GameObject soundPrefab;
    public GameObject laserPrefab;
    

    public void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        
    }

    //Koristenje lasera, kolizija s protivnicima, maxTargets uracunat
    public override void UseAbility()
    {
        if(! isOnCooldown()) {
            Vector2 origin = transform.position;
            
            Vector2 endPosition = transform.position + transform.right * laserLength;

            Vector2 direction = -origin + endPosition;
            direction.Normalize();
            
            StartCoroutine(ShotEffect());
            lineRenderer.SetPosition(0, origin);

            RaycastHit2D[] hit = Physics2D.RaycastAll(origin, direction, laserLength);

            
            lineRenderer.SetPosition(1, endPosition);

            for(int i = 0; i < maxTargets && i < hit.Length; i++)
            {
                if (hit[i].collider.gameObject.tag == "Enemy")
                {
                    hit[i].rigidbody.AddForce(-hit[i].normal * hitForce);
                }
            }
                
        }
        this.timeLastUsed = Time.time;
    }

    //Efekt pucanja, postavlja coolDown na duljinu pucnja trenutno(kasnije promijeniti po potrebi)
    private IEnumerator ShotEffect()
    {
        lineRenderer.enabled = true;
        yield return shotDuration;
        lineRenderer.enabled = false;
    }
}
