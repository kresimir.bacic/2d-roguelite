﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAbilityManager : MonoBehaviour
{
    public Ability Ability1;
    public Ability Ability2;


    public void UseAbility1()
    {
        if(!Ability1.isOnCooldown())
        {
            Ability1.UseAbility();
        }
        
    }

    public void UseAbility2()
    {
        Ability2.UseAbility();
    }

}
