﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public int maxHealth = 100;
    public float walkingSpeed = 1.0f;

    int currentHealth = 0;  //  Leave at 0 to initialize with maximum health

    private void Start()
    {
        if (currentHealth == 0) currentHealth = maxHealth;
    }
}
