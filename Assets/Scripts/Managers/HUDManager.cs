﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public Text healthText;
    public Text keyText;

    private Health trackedHealth;
    private GameManager managerComponent;

    private void OnEnable()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("GameManager");
        managerComponent = manager.GetComponent<GameManager>();
        managerComponent.OnKeysChanged += HandleOnKeysChanged;
        HandleOnKeysChanged(manager.GetComponent<GameManager>().GetKeys());
    }

    private void OnDisable()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("GameManager");
        manager.GetComponent<GameManager>().OnKeysChanged -= HandleOnKeysChanged;
    }

    public void TrackHealth(Health objectHealth)
    {
        objectHealth.OnHealthChanged += HandleOnHealthChanged;
        trackedHealth = objectHealth;
        HandleOnHealthChanged(objectHealth.GetHealth(), objectHealth.GetMaxHealth());
    }

    public void UntrackHealth()
    {
        trackedHealth.OnHealthChanged -= HandleOnHealthChanged;
    }

    void HandleOnHealthChanged(int currentHealth, int maxHealth)
    {
        healthText.text = currentHealth.ToString() + "/" + maxHealth.ToString();
        if(currentHealth <= 0 && !managerComponent.switching)
        {
            GameManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            manager.HandlePlayerDeath();
        }
    }

    void HandleOnKeysChanged(int currentKeys)
    {
        keyText.text = currentKeys.ToString();
    }
}
