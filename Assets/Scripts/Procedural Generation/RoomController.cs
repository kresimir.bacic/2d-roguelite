﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    public GameObject[] enemyGroupPrefabs;

    private GameObject enemyGroupInstance;
    private bool cleared = false;
    private int enemyGroupIndex = -1;

    private GameObject enemySpawngroup;

    private DoorController[] doors;

    private GameManager manager;

    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        doors = this.GetComponentsInChildren<DoorController>(true);
        if (enemyGroupPrefabs.Length > 0)
        {
            enemyGroupIndex = Random.Range(0, enemyGroupPrefabs.Length);
            enemySpawngroup = enemyGroupPrefabs[enemyGroupIndex];

            foreach (DoorController door in doors)
            {
                door.Init();
                door.SetOpen(false);
            }
        }
        else
        {
            enemySpawngroup = null;

            cleared = true;
            foreach (DoorController door in doors)
            {
                door.Init();
                door.SetCleared(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if(enemyGroupInstance == null && !cleared && manager.switching == false)
            {
                SpawnEnemyGroup();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if (enemyGroupInstance != null && manager.switching == false)
            {
                DespawnEnemyGroup();
            }
        }
    }

    void SpawnEnemyGroup()
    {
        enemyGroupInstance = Instantiate(enemySpawngroup, this.gameObject.transform);
        enemyGroupInstance.GetComponent<EnemyGroup>().OnGroupCleared += HandleGroupCleared;
    }

    void DespawnEnemyGroup()
    {
        enemyGroupInstance.GetComponent<EnemyGroup>().OnGroupCleared -= HandleGroupCleared;
        Destroy(enemyGroupInstance);
    }

    void HandleGroupCleared()
    {
        cleared = true;
        foreach(DoorController d in doors)
        {
            d.SetCleared(cleared);
        }
    }

    private void ReplaceEnemyGroup(GameObject enemyGroupPrefab)
    {
        enemySpawngroup = enemyGroupPrefab;
    }

    private void ActivateHatch()
    {
        foreach(DoorController d in doors)
        {
            if(d.gameObject.CompareTag("NextLevelHatch"))
            {
                d.gameObject.SetActive(true);
                break;
            }
        }
    }

    public void DesignateBossRoom(GameObject bossPrefab)
    {
        ReplaceEnemyGroup(bossPrefab);
        ActivateHatch();
    }
}
