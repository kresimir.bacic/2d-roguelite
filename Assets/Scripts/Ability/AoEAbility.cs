﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoEAbility : Ability
{



    public float radius;   //Radius within the enemies are damaged
    public float force;    // Magnitude of force that enemies are thrown from the blast
    public float damage;

    public float delay;    // Delay of an explosion/effect


    public GameObject soundPrefab;          //Auidoclip of explosion/blast
    public GameObject explosionPrefab;      //Prefab of explosion effect




    // Start is called before the first frame update
    void Start()
    {
                         

        BoxCollider2D bc;                                              
        bc = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;  //Box collider of the area of effect
        bc.size = new Vector2(radius, radius); 
        bc.isTrigger = true;

               
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Enemy")
        {
            
            Invoke("UseAbility", delay);     //Invoke after 'delay' seconds


        }
    
    } 
    public override void UseAbility()
    {
        if (!isOnCooldown())
        {
            

            Collider2D[] coliders = Physics2D.OverlapCircleAll(transform.position, radius);     //Find all the enemies colliders within the radius 
            foreach (Collider2D nearByObject in coliders)
            {
                Rigidbody2D rb = nearByObject.GetComponent<Rigidbody2D>();
                if (rb != null && rb.tag == "Enemy")                                 // Check if the Enemy has a rigidbody
                {
                      //Apply damage to the enemies 
                }
                Vector3 deltaPos = rb.transform.position - transform.position;    //Direction in which the (push) force will be applied
                Vector3 appliedForce = deltaPos.normalized * force;               
                rb.AddForce(appliedForce);                                       
            }

            _ = Instantiate(explosionPrefab, transform.position, transform.rotation);  //Instantiating the explosion effect

            _ = Instantiate(soundPrefab, transform.position, transform.rotation);      //Instantiating the explosion sound

            this.timeLastUsed = Time.time;
        }
    }

}
