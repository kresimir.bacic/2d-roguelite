﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour

{
    public float speed;
    public float range;
    public int damage;
    public string target;

    public AudioClip projectileHit;

    public Vector2 startingPosition;



    // Check if projectile reached it's range and destroy it
    void FixedUpdate()
    {

        if(Vector2.Distance(startingPosition, this.transform.position) >= range)
        {
            Destroy(gameObject);
        }
    }

    // Check if projectile has left screen
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    
    // Check if projectile collided with object with tag "Object"
    private void OnTriggerEnter2D(Collider2D collision)
    {
        {
            if (collision.gameObject.CompareTag("Object"))
            {
                AudioSource.PlayClipAtPoint(projectileHit, transform.position, 0.6f);
                Destroy(gameObject);
            }
            else if(collision.gameObject.CompareTag(target))
            {
                AudioSource.PlayClipAtPoint(projectileHit, transform.position, 0.6f);
                collision.gameObject.GetComponent<Health>().DealDamage(damage);
                Destroy(this.gameObject);
            }
        }
    }

}
