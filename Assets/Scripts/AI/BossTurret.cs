﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurret : MonoBehaviour
{
    public GameObject projectile;
    public Transform firingPoint;

    public float fireDelay;
    public float cooldown;

    public int numberOfProjectiles = 4;
    public float projectileDistance = 50f;

    private Vector2 myTransform;
    private Vector2 aimvector;
    private float aimAngle;
    private float timeLastUsed;

    private void Start()
    {
        timeLastUsed = Time.time - cooldown / 2;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > (timeLastUsed + cooldown))
        {
            myTransform = transform.position;
            Vector2 targetPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
            aimvector = targetPosition - myTransform;

            aimAngle = (Vector2.SignedAngle(transform.right, aimvector));

            timeLastUsed = Time.time;
            StartCoroutine("ShootAndWait");
        }
    }

    private void Attack(float shootAngle)
    {

        float angleStep = 360f / numberOfProjectiles;
        float angle = shootAngle;

        for (int i = 0; i < numberOfProjectiles; i++)
        {
            float projectileDirX = myTransform.x + Mathf.Sin((angle * Mathf.PI) / 180) * projectileDistance;
            float projectileDirY = myTransform.y + Mathf.Cos((angle * Mathf.PI) / 180) * projectileDistance;

            Vector2 projectileVector = new Vector2(projectileDirX, projectileDirY);
            Vector2 projectileMoveVector = (projectileVector - myTransform).normalized;

            var proj = Instantiate(projectile, firingPoint.transform.position, Quaternion.Euler(0f, 0f, angle));

            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveVector.x, projectileMoveVector.y);

            Destroy(proj, projectileDistance);

            angle += angleStep;
        }
    }

    private IEnumerator ShootAndWait()
    {
        Health bossHealth = this.GetComponent<Health>();

        if(bossHealth != null && bossHealth.currentHealth > 0)
        {
            Attack(aimAngle);
        }

        yield return new WaitForSeconds(fireDelay);

        if(bossHealth != null && bossHealth.currentHealth > 0)
        {
            Attack(aimAngle + 10f);
        }

        yield return new WaitForSeconds(fireDelay);

        if (bossHealth != null && bossHealth.currentHealth > 0)
        {
            Attack(aimAngle + 20f);
        }
    }
}
