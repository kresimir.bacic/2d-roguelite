﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MojProjectile : MonoBehaviour
{
    public bool dealsDamage = true;
    public int damage = 0;

    public bool hasKnockback = true;
    public float knockback = 0f;

    public float timeOfLife;
    public Animator projectileAnimator;

    private float spawnTime;
    private bool fadingAway = false;

    public void Start()
    {
        spawnTime = Time.time;
    }

    public void Update()
    {
        if (!fadingAway)
        {
            float currentTime = Time.time;
            if ((currentTime - spawnTime) > timeOfLife) fadeAway();
        }
    }

    public void onHit()
    {

    }
    
    //  Needs to be called when the projectile has reached its max range. Animates the projectile accordingly and then destroys it when the animation ends.
    public void fadeAway()
    {
        projectileAnimator.SetBool("FadeAway", true);
        float time = projectileAnimator.GetCurrentAnimatorStateInfo(0).length;
        Destroy(this, time);
    }

}
