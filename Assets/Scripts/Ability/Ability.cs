﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : MonoBehaviour
{
    public string abilityName;
    public string abilityDescription;
    public bool automaticFire = false;  //  Allows automatic fire based on attack speed (cooldown) by holding down the assigned button
    public float cooldown = 0.0f;

    protected bool onCooldown = false;
    protected float timeLastUsed = 0.0f;  //  Last time (in seconds) the ability was used. If (timeLastUsed - timeCurrent) >= cooldown, set onCooldown to false and refresh HUD


    public abstract void UseAbility();

    public bool isOnCooldown()
    {
        return onCooldown;
    }


    protected void FixedUpdate()
    {
        onCooldown = Time.time - timeLastUsed <= cooldown;
    }
}
