﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEndMarker : MonoBehaviour
{
    //  A purely marker component class marking a room as a "dead end" (only one door), useful for determining the level exit room.
}
