﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBuffAbility : Ability
{
    public float speedBuffMultiplier = 1.5f;
    public float durationInSeconds = 2.0f;

    public AudioClip buffAudioClip;

    public override void UseAbility()
    {
        this.timeLastUsed = Time.time;
        AudioSource.PlayClipAtPoint(buffAudioClip, this.transform.position);
        StartCoroutine("Buff");
    }

    IEnumerator Buff()
    {
        PlayerController pc = this.GetComponentInParent<PlayerController>();
        float oldWalkSpeed = pc.walkSpeedModifier;

        pc.walkSpeedModifier *= speedBuffMultiplier;
        yield return new WaitForSeconds(durationInSeconds);
        pc.walkSpeedModifier = oldWalkSpeed;
    }
}
