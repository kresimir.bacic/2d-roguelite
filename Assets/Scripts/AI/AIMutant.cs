﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMutant : MonoBehaviour
{
    public Rigidbody2D rb;

    public float moveSpeed = 1.0f;
    public float bounceBackForce = 2.0f;
    public int damage = 1;

    public AudioClip hitClip;
    public float hitVolume = 1.0f;

    private Transform target;
    private Vector2 myTransform;
    private float distance;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform.position;

        if (!rb) rb = this.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        target = go.transform;

        distance = Vector2.Distance(target.position, transform.position); // Zelimo li da krene cim se pojavi u sceni? Ili tek kada igrac dode u neki radijus

        Vector2 direction = target.position - transform.position;
        direction.Normalize();

        SpriteRenderer[] renderers = this.GetComponentsInChildren<SpriteRenderer>();
        if(direction.x >= 0)
        {
            foreach(SpriteRenderer r in renderers)
            {
                r.flipX = false;
            }
        }
        else
        {
            foreach(SpriteRenderer r in renderers)
            {
                r.flipX = true;
            }
        }

        if (moveSpeed != 0.0)
        {
            rb.AddForce(direction * moveSpeed);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(hitClip, transform.position);

            other.gameObject.GetComponent<Health>().DealDamage(damage);

            Vector2 direction = target.position - transform.position;
            direction.Normalize();
            
            rb.AddForce(-1 * direction * bounceBackForce, ForceMode2D.Impulse);

        }
        
    }
}
