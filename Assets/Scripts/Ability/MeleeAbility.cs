﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAbility : Ability
{
    public int damage;
    public float knockbackModifier = 30;

    public GameObject weaponSprite;
    public CapsuleCollider2D weaponEffectCollider;
    public Animator weaponSwingAnimator;

    public Transform knockbackCenter;

    public AudioClip swingSound;
    public AudioClip wallHitSound;
    public AudioClip entityHitSound;

    public override void UseAbility()
    {
        if (!isOnCooldown())
        {
            Vector2 aimVector = PlayerController.GetAimVector();
            float capsuleAngle = Mathf.Atan2(aimVector.y, aimVector.x) * Mathf.Rad2Deg;

            Collider2D[] colliders = Physics2D.OverlapCapsuleAll(weaponEffectCollider.gameObject.transform.position, weaponEffectCollider.size, weaponEffectCollider.direction, capsuleAngle);     //Find all the enemies colliders within the radius 

            AudioSource.PlayClipAtPoint(swingSound, weaponEffectCollider.gameObject.transform.position);

            if (!weaponSwingAnimator.GetCurrentAnimatorStateInfo(0).IsName("ArmPunch"))
            {
                weaponSwingAnimator.Play("ArmPunch", 0);
            }

            Animator spriteAnim;
            if ((spriteAnim = weaponSprite.GetComponent<Animator>()) != null)
            {
                spriteAnim.Play("ArmExtend");
            }

            foreach (Collider2D nearByObject in colliders)
            {
                if(nearByObject.gameObject.CompareTag("Enemy") || nearByObject.gameObject.CompareTag("Crate"))
                {
                    AudioSource.PlayClipAtPoint(entityHitSound, nearByObject.gameObject.transform.position, 0.8f);
                    nearByObject.gameObject.GetComponent<Health>().DealDamage(damage);

                    if (nearByObject.CompareTag("Enemy"))
                    {
                        Vector2 direction = new Vector2(nearByObject.transform.position.x - knockbackCenter.position.x, nearByObject.transform.position.y - knockbackCenter.position.y);
                        direction.Normalize();

                        nearByObject.gameObject.GetComponent<Rigidbody2D>().AddForce(direction * knockbackModifier, ForceMode2D.Impulse);
                    }
                }                                   
                else if(nearByObject.gameObject.CompareTag("WeakWall"))
                {
                    AudioSource.PlayClipAtPoint(wallHitSound, nearByObject.gameObject.transform.position, 0.8f);
                    nearByObject.gameObject.GetComponent<WeakWall>().HeavyHit();
                }
            }

            this.timeLastUsed = Time.time;
        }
    }
}
