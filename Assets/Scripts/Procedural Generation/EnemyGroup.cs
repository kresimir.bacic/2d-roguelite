﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour
{
    public int initialEnemies = 0;

    public System.Action OnGroupCleared;

    private int enemyCount = 0;

    private void OnEnable()
    {
        Health[] enemyHealth = this.GetComponentsInChildren<Health>();
        if(enemyHealth.Length > 0)
        {
            enemyCount = enemyHealth.Length;
            foreach(Health e in enemyHealth)
            {
                e.OnHealthDepleted += HandleEnemyDeath;
            }
        }
    }

    private void OnDisable()
    {
        Health[] enemyHealth = this.GetComponentsInChildren<Health>();
        if(enemyHealth.Length > 0)
        {
            foreach(Health e in enemyHealth)
            {
                e.OnHealthDepleted -= HandleEnemyDeath;
            }
        }
    }

    private void HandleEnemyDeath(GameObject enemy)
    {
        enemy.GetComponent<Health>().OnHealthDepleted -= HandleEnemyDeath;
        enemyCount--;
        Destroy(enemy);

        if(enemyCount == 0 && OnGroupCleared != null)
        {
            OnGroupCleared();
        }
    }
}
