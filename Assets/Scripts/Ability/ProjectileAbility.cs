﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAbility : Ability
{
    public Transform firingPoint;
    public Projectile[] projectilePrefab;
    public float[] angle;
    public int maxProjectilesOnScreen = 0;
    public float speed = 400;
    public float range = 10;
    public int maximumAmmo = 10;
    public int ammo;

    public bool flip = true;

    public AudioClip shootAudioClip;
    public float shootVolume = 1.0f;

    public float replenishTime;
    public int replenishRate;


    void Start()
    {
        // Star with maximum ammo (?)
        ammo = maximumAmmo;


        // If ammo is replenished naturally, start coroutine
        if(replenishRate > 0)
        {
            StartCoroutine(Replenish());
        }

        // If projectile has more angles than prefabs, make missing prefabs last given prefab
        if(projectilePrefab.Length < angle.Length)
        {
            Projectile last = projectilePrefab[projectilePrefab.Length - 1];
            Projectile[] projectilePrefabTemp = new Projectile[angle.Length];

            for(int i = 0; i < projectilePrefab.Length; i++)
            {
                projectilePrefabTemp[i] = projectilePrefab[i];
            }

            for(int i = projectilePrefab.Length; i < projectilePrefabTemp.Length; i++)
            {
                projectilePrefabTemp[i] = last;
            }

            projectilePrefab = projectilePrefabTemp;
        }
    }

    public override void UseAbility()
    {
        Projectile projectile;
        Vector2 aimvector = PlayerController.GetAimVector();

        // Check if projectile is max number of projectile is on screen; if maxProjectilesOnScreen is set to 0, it means infinite projectiles on screen are allowed
        if(maxProjectilesOnScreen == 0 || this.transform.childCount / angle.Length < maxProjectilesOnScreen)
        {

            // Check if there is ammo for projectile; if maximumAmmo is set to 0 it means infinite ammo
            if(maximumAmmo == 0 || ammo > 0)
            {
                if(maximumAmmo > 0)
                    ammo--;


                //  For cooldown purposes
                timeLastUsed = Time.time;

                AudioSource.PlayClipAtPoint(shootAudioClip, firingPoint.position, shootVolume);

                // Instantiate as much projectile as there are angles
                for (int i = 0; i < angle.Length; i++)
                {

                    // Get aim angle (need to be changed)
                    float aimAngle = (Vector2.SignedAngle(transform.right, aimvector));

                    float angleR = (angle[i] + aimAngle);
                    if (!PlayerController.IsFacingRight() && flip) angleR += 180;
                    projectile = Instantiate(projectilePrefab[i], firingPoint.position, Quaternion.Euler(0f, 0f, angleR));
                    projectile.startingPosition = projectile.transform.position;

                    angleR *= Mathf.Deg2Rad;

                    // If projectile range isn't defined use range of ProjectileAbility
                    if(projectile.range <= 0)
                    {
                        projectile.range = range;
                    }
                    

                    // If projectile speed isn't defined use speed of ProjectileAbility
                    if(projectile.speed <= 0)
                    {
                        projectile.speed = speed;
                    }

                    // Add force to projectile
                    projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(angleR), Mathf.Sin(angleR)) * projectile.speed);
                }
            }
        }

    }

    // Replenish ammo every x seconds
    IEnumerator Replenish()
    {
        for(; ; )
        {
            // execute block of code here
            if(ammo < maximumAmmo)
                ammo += replenishRate;
            yield return new WaitForSeconds(replenishTime);
        }
    }
    
    // Call fixed update from Ability class
    protected new void FixedUpdate()
    {
        base.FixedUpdate();
    }

}
