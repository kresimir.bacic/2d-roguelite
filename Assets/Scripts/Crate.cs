﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour
{
    public Health crateHealth;
    public GameObject dropPrefab;

    private void OnEnable()
    {
        crateHealth.OnHealthDepleted += HandleOnHealthDepleted;
    }

    private void OnDisable()
    {
        crateHealth.OnHealthDepleted -= HandleOnHealthDepleted;
    }

    void HandleOnHealthDepleted(GameObject go)
    {
        Debug.Log("Depleted!");
        Instantiate(dropPrefab, this.gameObject.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
