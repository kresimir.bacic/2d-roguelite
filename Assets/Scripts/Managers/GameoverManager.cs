﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameoverManager : MonoBehaviour
{
    public AudioClip gameoverMusic;

    private GameManager manager;
    private AudioSource cameraSource;

    private void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        manager.playing = false;

        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        cameraSource = camera.GetComponent<AudioSource>();

        InvokeRepeating("Fadeout", 0f, 0.05f);
        AudioSource.PlayClipAtPoint(gameoverMusic, cameraSource.transform.position);
    }

    public void HandleOnMainMenuPressed()
    {
        manager.DestroyScene();
        Time.timeScale = 1;
        manager.playing = false;
        SceneManager.LoadScene("MenuScene");
    }

    IEnumerator Fadeout()
    {
        if (cameraSource.volume < 0.05)
        {
            yield return null;
        }
        else
        {
            cameraSource.volume -= 0.08f;
            yield return null;
        }
    }
}
