﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{
    public DoorType door = DoorType.REGULAR;
    public Direction doorDirection;
    public GameObject securityAddon;
    public bool reachable = true;
    public bool alwaysRegular = false;

    public AudioClip doorOpening;
    public AudioClip keycardOpening;

    public GameObject storyPrefab;

    private bool open = false;
    private bool cleared = false;

    public void Init()
    {
        int rand = Random.Range(0, 2);
        if(!alwaysRegular && rand == 1)
        {
            door = DoorType.SECURITY;
            GameObject.FindGameObjectWithTag("Jigsaw").GetComponent<Jigsaw>().SpawnKey(reachable);
        }
        else
        {
            door = DoorType.REGULAR;
            if(securityAddon != null) Destroy(securityAddon);
        }
    }

    public void SetOpen(bool b)
    {
        open = b;
        AudioSource.PlayClipAtPoint(doorOpening, transform.position);
        this.GetComponentsInChildren<Animator>()[0].SetBool("Open", b);
        this.GetComponentsInChildren<Animator>()[1].SetBool("Open", b);
        if(securityAddon != null)
        {
            AudioSource.PlayClipAtPoint(keycardOpening, transform.position);
            securityAddon.GetComponent<Animator>().Play("DoorSecurityOpening");
        }
    }

    public void SetCleared(bool b)
    {
        cleared = b;
        if(this.door == DoorType.REGULAR)
        {
            this.SetOpen(b);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            switch(door)
            {
                case DoorType.REGULAR:
                    if(open) TransferRooms(collision.gameObject);
                    break;
                case DoorType.SECURITY:
                    if(cleared && !open)
                    {
                        // Ako je lik ovlašten ili ima barem jedan ključ, otvori sigurnosna vrata
                        if (collision.gameObject.GetComponent<Health>().authorized) this.SetOpen(true);
                        else if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().SpendKey())
                        {
                            this.SetOpen(true);
                        }
                    }
                    else if(open) TransferRooms(collision.gameObject);
                    break;
                default:
                    break;
            }
        }
    }

    private void TransferRooms(GameObject player)
    {
        switch (doorDirection)
        {
            case Direction.DIRECTION_LEFT:
                player.transform.position += new Vector3(-1.45f, 0, 0);
                break;
            case Direction.DIRECTION_RIGHT:
                player.transform.position += new Vector3(1.45f, 0, 0);
                break;
            case Direction.DIRECTION_UP:
                player.transform.position += new Vector3(0, 2.1f, 0);
                break;
            case Direction.DIRECTION_DOWN:
                player.transform.position += new Vector3(0, -2f, 0);
                break;
            case Direction.NO_DOOR_DOWN:
                Instantiate(GameObject.FindGameObjectWithTag("Jigsaw").GetComponent<Jigsaw>().storyCanvas);
                break;
            default:
                break;
        }
    }
}

public enum DoorType
{
    REGULAR,
    SECURITY
}
