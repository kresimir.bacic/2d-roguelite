﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityManager : MonoBehaviour
{
    public Ability mouseLeft;
    public Ability mouseRight;
    public Ability spaceKey;
    public Ability fKey;

    void Update()
    {
        ProcessAbilityInputs();
    }

    private void ProcessAbilityInputs()
    {
        //  MouseLeft input
        if(Input.GetKeyDown(KeyCode.Mouse0) || (Input.GetKey(KeyCode.Mouse0) && mouseLeft.automaticFire == true))
        {
            if (!mouseLeft.isOnCooldown()) mouseLeft.UseAbility();
        }

        //  MouseRight input
        if(Input.GetKeyDown(KeyCode.Mouse1) || (Input.GetKey(KeyCode.Mouse1) && mouseRight.automaticFire == true))
        {
            if (!mouseRight.isOnCooldown()) mouseRight.UseAbility();
        }

        //  Space input
        if(Input.GetKeyDown(KeyCode.Space) || (Input.GetKey(KeyCode.Space) && spaceKey.automaticFire == true))
        {
            if (!spaceKey.isOnCooldown()) spaceKey.UseAbility();
        }

        //  F key
        if(Input.GetKeyDown(KeyCode.F) || (Input.GetKey(KeyCode.F) && fKey.automaticFire == true))
        {
            if (!fKey.isOnCooldown()) fKey.UseAbility();
        }
    }
}
