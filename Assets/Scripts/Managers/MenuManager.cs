﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject menuScreen;
    public GameObject selectScreen;

    public Text characterNameText;
    public Text characterDescriptionText;

    public GameObject AIPrefab;
    public string AIDescription;

    public GameObject cyborgPrefab;
    public string cyborgDescription;

    private GameObject selected;

    private void Start()
    {
        OnAIButtonPressed();
    }

    public void OnPlayButtonPressed()
    {
        menuScreen.SetActive(false);
        selectScreen.SetActive(true);
    }

    public void OnQuitButtonPressed()
    {
        Application.Quit();
    }

    public void OnStartButtonPressed()
    {
        GameManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        manager.ChangeCharacter(selected);
        SceneManager.LoadScene("FactoryLevel");
    }

    public void OnBackButtonPressed()
    {
        selectScreen.SetActive(false);
        menuScreen.SetActive(true);
    }

    public void OnAIButtonPressed()
    {
        selected = AIPrefab;
        characterNameText.text = "Artificial Intelligence";
        characterDescriptionText.text = AIDescription;
    }

    public void OnCyborgButtonPressed()
    {
        selected = cyborgPrefab;
        characterNameText.text = "Cyborg";
        characterDescriptionText.text = cyborgDescription;
    }
}
