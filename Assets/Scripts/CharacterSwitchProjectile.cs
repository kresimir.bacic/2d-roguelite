﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSwitchProjectile : Projectile
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && collision.gameObject.GetComponent<Health>().isMachine)
        {

            // Promijeni prefab igrača u manageru
            GameManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            manager.ChangeCharacter(collision.gameObject.GetComponent<PlayerPrefab>().playerPrefab);

            manager.switching = true;

            // Zapamti pozicije igrača i neprijatelja
            Vector3 oldPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
            Vector3 newPosition = collision.gameObject.transform.position;

            // Zapamti health neprijatelja i, ako je manji ili jednak 0, postavi ga na 1
            Health enemyHealth = collision.gameObject.GetComponent<Health>();
            int newHealth = enemyHealth.GetHealth();
            if (newHealth <= 0)
            {
                newHealth = 1;
            }

            // "Ubij" neprijatelja
            if(enemyHealth.OnHealthDepleted != null)
            {
                enemyHealth.OnHealthDepleted(collision.gameObject);
            }

            // Instanciraj igrača preko GameManagera
            manager.SpawnCharacter(newPosition);

            // Osvježi mu health
            Health playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
            int damage = playerHealth.GetMaxHealth() - newHealth;
            playerHealth.DealDamage(damage);


            StartCoroutine("DelayedUpdate", playerHealth);

            manager.switching = false;

            // Uništi projektil
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Object"))
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator DelayedUpdate(Health playerHealth)
    {
        yield return new WaitForSeconds(0.1f);

        if (playerHealth.OnHealthChanged != null)
        {
            playerHealth.OnHealthChanged(playerHealth.currentHealth, playerHealth.maxHealth);
        }
    }

}
