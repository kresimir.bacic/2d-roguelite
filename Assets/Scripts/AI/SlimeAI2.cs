﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAI2 : MonoBehaviour
{


    Rigidbody2D slimeRigidbody2D;
    Animator slimeAnimator;
  
    public float speed = 5f;
    public float projectileSpeed = 10f;
    public GameObject projectile;
    public float walkTime = 3;            // vrijeme x koje hoda prije nego što stane i zapuca (zapuca svakih 'walkTime' sekundi)
    public float shootingTime = 0.5f;     //vrijeme koje stoji u mjestu i puca
    public float range = 5f;                //vrijeme zivota projectila
    public int numberOfProjectiles = 1;
    public float projectileDistance = 50f;

    public AudioClip slimeShootClip;
    public float slimeShootVolume = 1.0f;


    private Vector2 direction;
    private Vector2 ProjDirection;

    private float tempSpeed;        //pomocna varijabla za ocuvanje vrijednosti brzine 
    private float timer = 0.0f;           //pomocni timer
    private bool facingRight = true;

    private GameObject Player;


    // Start is called before the first frame update
    void Start()
    {
        if (!slimeRigidbody2D)
        {
            slimeRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
            slimeRigidbody2D.freezeRotation = true;                   //sprijecava rotaciju pri sudaru s drugim objekitma
        }

        if (!slimeAnimator) slimeAnimator = this.GetComponentInChildren<Animator>();

        walkTime += Time.deltaTime;                                  //postavljanje timera za hodanje i pucanje
        shootingTime += Time.deltaTime;

    }

    // Update is called once per frame
    void Update()
    {
        Player = GameObject.FindGameObjectWithTag("Player");         //nadi igraca 
        direction = (Player.transform.position - transform.position).normalized;  //smjer u kojem se nalazi igrac
        if ((direction.x < 0 && facingRight) || (direction.x >= 0 && !facingRight)) this.facingRight = !facingRight;
        this.GetComponentInChildren<SpriteRenderer>().flipX = !facingRight;

        timer += Time.deltaTime;                                                  //pomocni timer za hodanje

        if (timer > walkTime)
        {
            timer = timer - walkTime;                                             //novo postavljanje timera
            tempSpeed = speed;
            speed = 0;

            StartCoroutine(Wait());                                               //stoji na mjestu

            Attack();                                                  
        }
        else
        {
             slimeRigidbody2D.velocity = new Vector2(direction.x, direction.y) * speed;    //kretanje slime-a
        }

    }

    private void Attack()
    {
        float angleStep = 360f / numberOfProjectiles;
        float angle = 0f;

        Player = GameObject.FindGameObjectWithTag("Player");
        ProjDirection = (Player.transform.position - this.transform.position).normalized;    //smjer u kojem je ispucan projectile
        List<GameObject> projectiles = new List<GameObject>();

        AudioSource.PlayClipAtPoint(slimeShootClip, transform.position);

        for (int i = 0; i < numberOfProjectiles; i++)
        {
            angle = i * angleStep;

            projectiles.Add(Instantiate(projectile, transform.position, Quaternion.identity));   //ako je vrijeme za pucanje, stvori novi bullet

            Rigidbody2D proj_rb = projectiles[i].GetComponent<Rigidbody2D>();

            float projectileX = ProjDirection.x + Mathf.Sin((angle * Mathf.PI) / 180) * projectileDistance;
            float projectileY = ProjDirection.y + Mathf.Cos((angle * Mathf.PI) / 180) * projectileDistance;

            proj_rb.velocity = (new Vector2(projectileX, projectileY)).normalized * projectileSpeed;
            Destroy(projectiles[i], projectileDistance);
        }
    }


    //korutina za stanjanje i cekanje u mjestu
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(shootingTime);
        speed = tempSpeed;                                          //postavnjanje brzine
    }
}
