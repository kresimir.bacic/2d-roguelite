﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public int roomsRemaining;
    public RoomBasis roomPrefabs;

    //private RoomTemplates templates;    //  Component containing all possible room templates
    private List<GameObject> spawners;  //  All currently existing spawners
    private List<GameObject> rooms;

    /*
    private List<GameObject> topRooms;
    private List<GameObject> bottomRooms;
    private List<GameObject> leftRooms;
    private List<GameObject> rightRooms;
    */

    void Start()
    {
        //templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        rooms = new List<GameObject>();
        rooms.AddRange(roomPrefabs.rooms);

        spawners = new List<GameObject>();
        spawners.AddRange(GameObject.FindGameObjectsWithTag("SpawnPoint"));
        //Extract();

        while(spawners.Count > 0)
        {
            //  Dictionary containing all 'must-have' door constraints from the RoomSpawner components
            Dictionary<Vector3, List<Direction>> spawnerPositions = new Dictionary<Vector3, List<Direction>>();
            //  Iterate on all spawners present in the scene
            foreach(GameObject s in spawners)
            {
                if(!spawnerPositions.ContainsKey(RoundVector(s.transform.position)))
                {
                    //  If a spawner's position isn't in the dictionary, create a new entry and add its constraint to the list
                    List<Direction> constraints = new List<Direction>();
                    spawnerPositions.Add(RoundVector(s.transform.position), constraints);
                    constraints.Add(s.GetComponent<RoomSpawner>().openingDirection);
                }
                else
                {
                    //  If a spawner's position is already in the dictionary, add its constraint to the list
                    spawnerPositions[RoundVector(s.transform.position)].Add(s.GetComponent<RoomSpawner>().openingDirection);
                }
            }

            //  Contains all RoomCenter objects
            List<GameObject> roomCenters = new List<GameObject>();
            roomCenters.AddRange(GameObject.FindGameObjectsWithTag("RoomCenter"));
            Debug.Log(roomCenters.Count + " rooms spawned.");

            //  HashSet contains all unique RoomCenter positions
            HashSet<Vector3> roomPositions = new HashSet<Vector3>();
            foreach(GameObject r in roomCenters)
            {
                roomPositions.Add(RoundVector(r.transform.position));
            }
            foreach (Vector3 v in roomPositions) Debug.Log("Center position: " + v);

            //  Iterate on all unique spawner positions and compare them to existing RoomCenter positions
            //  to generate 'must-not-have' constraints
            foreach(Vector3 k in spawnerPositions.Keys)
            {
                Debug.Log("Spawner position: " + k);
                int reservedRooms = spawnerPositions[k].Count;
                Debug.Log(reservedRooms + " rooms already reserved!");
                //  If there isn't a 'must-have' door constraint in a given direction, check if a room exists
                //  in that position and, if it does, add a 'must-not-have' constraint
                if (!spawnerPositions[k].Contains(Direction.DIRECTION_DOWN))
                {
                    if(roomPositions.Contains(k +(new Vector3(0, -4, 0))))
                    {
                        spawnerPositions[k].Add(Direction.NO_DOOR_DOWN);
                    }
                }
                if(!spawnerPositions[k].Contains(Direction.DIRECTION_LEFT))
                {
                    if (roomPositions.Contains(k + (new Vector3(-4, 0, 0))))
                    {
                        spawnerPositions[k].Add(Direction.NO_DOOR_LEFT);
                    }
                }
                if(!spawnerPositions[k].Contains(Direction.DIRECTION_RIGHT))
                {
                    if (roomPositions.Contains(k + (new Vector3(4, 0, 0))))
                    {
                        spawnerPositions[k].Add(Direction.NO_DOOR_RIGHT);
                    }
                }
                if(!spawnerPositions[k].Contains(Direction.DIRECTION_UP))
                {
                    if (roomPositions.Contains(k + (new Vector3(0, 4, 0))))
                    {
                        spawnerPositions[k].Add(Direction.NO_DOOR_UP);
                    }
                }
                Debug.Log("Attempting spawn...");
                //  Spawn a room with defined constraints
                SpawnRoom(k, spawnerPositions[k], 0);
                Debug.Log("Yay!");
            }

            //  Clear the spawner list and reinitialize it with newly created spawners
            spawners.Clear();
            spawners.AddRange(GameObject.FindGameObjectsWithTag("SpawnPoint"));

            //  Wait a bit, giving time for the spawners colliding with room center objects to self-destruct
            StartCoroutine(Coroutine());
        }
    }

    //  Converts arrays in templates to local lists for expanded functionality
    void Extract()
    {
        /*
        topRooms.AddRange(templates.topRooms);
        bottomRooms.AddRange(templates.bottomRooms);
        leftRooms.AddRange(templates.leftRooms);
        rightRooms.AddRange(templates.rightRooms);
        */
    }

    //  Rounds three-dimensional vectors to integer values to enable GameObject position comparison
    Vector3 RoundVector(Vector3 original)
    {
        Vector3 rounded = new Vector3(Mathf.Round(original.x), Mathf.Round(original.y), Mathf.Round(original.z));
        return rounded;
    }

    //  Instantiates a random room prefab from a list of prefabs containing needed constraints
    void SpawnRoom(Vector3 position, List<Direction> constraints, int reservedRooms)
    {
        //  A temporary list to hold all rooms deemed adequate
        List<GameObject> adequateRooms = new List<GameObject>();
        adequateRooms.AddRange(rooms);

        //  If a room doesn't fulfill all given conditions, remove it from the list
        foreach(GameObject r in rooms)
        {
            Debug.Log("Rooms remaining: " + adequateRooms.Count);
            //  Current room's constraints
            List<Direction> roomConst = new List<Direction>();
            roomConst.AddRange(r.GetComponent<RoomConstraints>().constraints);
            bool adequate = true;

            //  Check if the room prefab fulfills all conditions
            foreach(Direction c in constraints)
            {
                //  If it doesn't, it's inadequate
                if(!roomConst.Contains(c))
                {
                    adequate = false;
                    Debug.Log(r.name + " doesn't fit " + c.ToString());
                    break;
                }
                Debug.Log(r.name + " fits " + c.ToString());
            }

            //  If adding the room would exceed the previously defined room number, it's inadequate
            int newRooms = r.GetComponent<RoomConstraints>().doorCount - reservedRooms;
            if(newRooms > roomsRemaining)
            {
                Debug.Log(r.name + " creates " + newRooms + " rooms, but only " + roomsRemaining + " rooms remain! Inadequate!");
                adequate = false;
            }

            //  If it's inadequate, remove the room from the list of adequate rooms
            if(!adequate)
            {
                adequateRooms.Remove(r);
            }
        }
        Debug.Log("Adequate rooms left: " + adequateRooms.Count);
        //  Instantiate a random room from the adequate rooms list
        int rand = Random.Range(0, adequateRooms.Count);
        roomsRemaining = roomsRemaining - adequateRooms[rand].GetComponent<RoomConstraints>().doorCount + reservedRooms;
        Instantiate(adequateRooms[rand], position, Quaternion.identity);
    }

    private IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(2f);
    }
}
