﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityScripts
{
    public static void FlipObject(Transform objectTransform, bool flipped)
    {
        float flip = 180f;
        if (flipped)
        {
            flip = 0f;
            flipped = false;
        }
        else flipped = true;
        objectTransform.rotation = Quaternion.AngleAxis(flip, Vector3.up);
    }
}
