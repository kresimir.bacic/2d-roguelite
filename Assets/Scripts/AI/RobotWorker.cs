﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotWorker : MonoBehaviour
{
    // Flag for activation
    bool active = true;

    // Delay between activations
    public float delay = 1f;

    // Timer between activations
    float timer = 0.3f;

    // Speed for moving towards player
    public float speed = 3;
    public float chargeTime = 2.0f;

    public float bounceBackForce = 2.0f;
    public int damage = 1;

    public Rigidbody2D rigid;

    // Transform of player
    Transform target;

    public AudioClip hitClip;
    public float hitVolume = 1.0f;

    private float activeStart;
    private Vector2 dir;

    private bool facingRight = true;

    void Update()
    {
	    // Set player as target
	    target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

	    // Timer for activation of active state
	    if(timer <= 0)
        {
		    active = true;
            activeStart = Time.time;
            dir = (target.position - this.transform.position).normalized;

            if((dir.x < 0 && facingRight) || (dir.x >= 0 && !facingRight))
            {
                UtilityScripts.FlipObject(this.gameObject.transform, !facingRight);
                facingRight = !facingRight;
            }

            timer = delay;
        } else {
		    timer -= Time.deltaTime;
	    }

        if(active && (Time.time > (activeStart + chargeTime)))
        {
            active = false;
            timer = delay;
            rigid.velocity = new Vector3(0, 0, 0);
        }

	// Move towards player
        if(active) {
            rigid.velocity = dir * speed;
	    }

	
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(hitClip, transform.position);

            other.gameObject.GetComponent<Health>().DealDamage(damage);

            Vector2 direction = target.position - transform.position;
            direction.Normalize();

            rigid.AddForce(-1 * direction * bounceBackForce, ForceMode2D.Impulse);

            active = false;
            timer = delay;
        }

    }
}
