﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public List<Sprite> sprites;
    public AudioClip audioClip;

    public int healAmount = 1;

    public System.Action OnKeyCollected;

    private static List<int> allCodes = new List<int>();
    private int code = -1;

    // Start is called before the first frame update
    void Start()
    {
        while (true)
        {
            //code = Random.Range(0, (sprites.Count * 2));
            code = Random.Range(0, sprites.Count);

            if(sprites.Count == allCodes.Count)
            {
                allCodes.Clear();
            }

            if (!allCodes.Contains(code))
            {
                allCodes.Add(code);
                break;
            }
        }

        SpriteRenderer renderer = this.gameObject.GetComponent<SpriteRenderer>();
        //int spriteIndex = Mathf.FloorToInt(code / 2);

        renderer.sprite = sprites[code];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(OnKeyCollected != null)
            {
                OnKeyCollected();
            }

            Health playerHealth = collision.gameObject.GetComponent<Health>();
            if(playerHealth.keysHeal)
            {
                playerHealth.HealHealth(healAmount);
            }

            AudioSource.PlayClipAtPoint(audioClip, transform.position);
            Destroy(this.gameObject);
        }
    }
}