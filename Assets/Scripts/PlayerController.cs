﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D playerRigidbody2D = null;
    public Animator playerAnimator = null;
    public GameObject weapon = null;
    public Animator weaponAnimator = null;

    public bool flippable = true;

    public float walkSpeedModifier = 1.0f;

    private Vector2 movementVector;
    private static Vector2 aimVector;
    private float minimalMagnitude = 0.05f;
    private static bool facingRight = true;

    private GameManager manager;

    //  Initialization
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        if(!playerRigidbody2D) playerRigidbody2D = this.GetComponent<Rigidbody2D>();
        if(!playerAnimator) playerAnimator = this.GetComponent<Animator>();
        if (!weaponAnimator && weapon) weaponAnimator = weapon.GetComponent<Animator>();
    }

    //  For non-physics based calculations
    void Update()
    {
        if(manager.playing)
        {
            ProcessInputs();
            Aim();
        }
    }

    //  For physics based calculations
    void FixedUpdate()
    {
        Move();
    }

    //  Processes all current inputs
    void ProcessInputs()
    {
        movementVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (movementVector.magnitude < minimalMagnitude)
        {
            movementVector = Vector2.zero;
            playerAnimator.SetBool("Moving", false);
            if(weaponAnimator != null) weaponAnimator.SetBool("Moving", false);
        }
        else
        {
            playerAnimator.SetBool("Moving", true);
            if(weaponAnimator) weaponAnimator.SetBool("Moving", true);
        }

        if (weapon) aimVector = Input.mousePosition - Camera.main.WorldToScreenPoint(weapon.transform.position);
        else aimVector = Input.mousePosition - Camera.main.WorldToScreenPoint(this.transform.position);
    }

    //  Moves the player character if movement keys are being pressed
    void Move()
    {
        if(!movementVector.Equals(Vector2.zero)) playerRigidbody2D.velocity = movementVector * walkSpeedModifier;
    }

    //  Rotates the weapon sprite depending on the vector between player and cursor position
    void Aim()
    {
        if ((aimVector.x < 0 && facingRight) || (aimVector.x >= 0 && !facingRight))
        {
            if(flippable) UtilityScripts.FlipObject(this.GetComponent<Transform>(), !facingRight);
            facingRight = !facingRight;
        }

        float angle = Mathf.Atan2(aimVector.y, aimVector.x) * Mathf.Rad2Deg;
        if(weapon) weapon.transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);    //  Reset rotation
        if(facingRight && weapon) weapon.transform.Rotate(0.0f, 0.0f, angle);
        else if(weapon) weapon.transform.Rotate(180.0f, 0.0f, -angle);
    }

    /*
     *  GETTER functions 
     */ 
    Vector2 GetMovementVector()
    {
        return movementVector;
    }

    public static Vector2 GetAimVector()
    {
        return aimVector;
    }

    public static bool IsFacingRight()
    {
        return facingRight;
    }
}
