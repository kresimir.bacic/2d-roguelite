﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameObject managerSingleton;
    public GameObject playerPrefab;

    public GameObject HUDPrefab;
    public GameObject pausePrefab;
    public GameObject deathPrefab;

    public System.Action<int> OnKeysChanged;

    private GameObject currentCharacter;

    private int keys = 0;
    private GameObject HUDObject = null;
    private GameObject pauseObject = null;
    private GameObject deathObject = null;
    public bool playing = false;
    public bool switching = false;

    // Start is called before the first frame update
    void Start()
    {
        if(managerSingleton != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            managerSingleton = this.gameObject;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(playing)
            {
                PauseGame();
            }
            else
            {
                UnpauseGame();
            }
        }
    }

    public void ChangeCharacter(GameObject characterPrefab)
    {
        playerPrefab = characterPrefab;
    }

    public void SpawnCharacter(Vector3 position)
    {
        if(currentCharacter != null)
        {
            HUDObject.GetComponent<HUDManager>().UntrackHealth();
            Destroy(currentCharacter);
        }

        currentCharacter = Instantiate(playerPrefab, position, Quaternion.identity);
        HUDObject.GetComponent<HUDManager>().TrackHealth(currentCharacter.GetComponent<Health>());
    }

    void HandleOnKeyCollected()
    {
        keys++;

        if(OnKeysChanged != null)
        {
            OnKeysChanged(keys);
        }
    }

    public bool SpendKey()
    {
        if(keys > 0)
        {
            keys--;

            if (OnKeysChanged != null)
            {
                OnKeysChanged(keys);
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public void CreateHUD()
    {
        if(HUDObject != null)
        {
            HUDObject.GetComponent<HUDManager>().UntrackHealth();
            Destroy(HUDObject);
        }

        HUDObject = Instantiate(HUDPrefab);
    }

    public void PauseGame()
    {
        playing = false;

        Time.timeScale = 0;
        pauseObject = Instantiate(pausePrefab);
    }

    public void UnpauseGame()
    {
        if(pauseObject != null)
        {
            Destroy(pauseObject);
            Time.timeScale = 1;

            playing = true;
        }
    }

    public int GetKeys()
    {
        return keys;
    }

    public void TrackKey(Key k)
    {
        k.OnKeyCollected += HandleOnKeyCollected;
    }

    public void InitScene()
    {
        keys = 0;
        CreateHUD();
        SpawnCharacter(new Vector3(0, 0, 0));
        HUDObject.GetComponent<HUDManager>().TrackHealth(currentCharacter.GetComponent<Health>());
        playing = true;
    }

    public void DestroyScene()
    {
        playing = false;
        if(HUDObject != null) HUDObject.GetComponent<HUDManager>().UntrackHealth();
        if(currentCharacter != null) Destroy(currentCharacter);
        if(HUDObject != null) Destroy(HUDObject);
    }

    public void HandlePlayerDeath()
    {
        playing = false;
        Time.timeScale = 0;
        deathObject = Instantiate(deathPrefab);
    }
}
