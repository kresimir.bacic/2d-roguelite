﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotGuardAI : MonoBehaviour
{
    public Rigidbody2D RigidBody;
    public Animator Animator;
    public GameObject Weapon;
    public Animator WeaponAnimator;

    private bool flipRight = true;
    private Vector2 aimVector;
    private Vector2 targetPosition;
    private float distance;

    public float enemyWalkSpeedModifier = 1.0f;

    public float minDistance;
    public float maxDistance;
    public float abilityChange;

    private EnemyAbilityManager abilityManager;

    private GameObject target;
    private Vector2 randomizedVector;
    private Vector2 velocityVector;
    private float timer = 1.5f;

    public Transform firingPoint;
    public Projectile[] projectilePrefab;
    public float[] angle;
    public int maxProjectilesOnScreen = 0;
    public float speed = 400;
    public float range = 10;
    public int maximumAmmo = 0;
    public int ammo;
    public float cooldown;

    public AudioClip shootAudioClip;
    public float shootVolume = 1.0f;

    private float timeLastUsed;


    protected void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");

        abilityManager = gameObject.GetComponent<EnemyAbilityManager>();

        InvokeRepeating("Randomize", 0f, 0.5f);

        timeLastUsed = Time.time + cooldown;
    }

    protected void Update()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        targetPosition = target.transform.position;
        distance = Vector2.Distance(this.transform.position, target.transform.position);
        
        Aim();
        Move();

        if(timer <= 0 && ((timeLastUsed + cooldown) < Time.time))
        {
            Shoot();
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    private void Shoot()
    {

        Projectile projectile;

        // Check if projectile is max number of projectile is on screen; if maxProjectilesOnScreen is set to 0, it means infinite projectiles on screen are allowed
        if (maxProjectilesOnScreen == 0 || this.transform.childCount / angle.Length < maxProjectilesOnScreen)
        {

            // Check if there is ammo for projectile; if maximumAmmo is set to 0 it means infinite ammo
            if (maximumAmmo == 0 || ammo > 0)
            {
                if (maximumAmmo > 0)
                    ammo--;


                //  For cooldown purposes
                timeLastUsed = Time.time;

                AudioSource.PlayClipAtPoint(shootAudioClip, firingPoint.position);

                // Instantiate as much projectile as there are angles
                for (int i = 0; i < angle.Length; i++)
                {

                    // Get aim angle (need to be changed)
                    float aimAngle = (Vector2.SignedAngle(transform.right, aimVector));

                    float angleR = (angle[i] + aimAngle);
                    if (!IsFacingRight()) angleR += 180;
                    projectile = Instantiate(projectilePrefab[i], firingPoint.position, Quaternion.Euler(0f, 0f, angleR));
                    projectile.startingPosition = projectile.transform.position;

                    angleR *= Mathf.Deg2Rad;

                    // If projectile range isn't defined use range of ProjectileAbility
                    if (projectile.range <= 0)
                    {
                        projectile.range = range;
                    }


                    // If projectile speed isn't defined use speed of ProjectileAbility
                    if (projectile.speed <= 0)
                    {
                        projectile.speed = speed;
                    }

                    // Add force to projectile
                    projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(angleR) * projectile.speed, Mathf.Sin(angleR) * projectile.speed));
                }
            }
        }

    }

    private void Move()
    {
        

        if(distance < minDistance)
        {
            velocityVector = aimVector * -1;
        } 
        if(distance > maxDistance)
        {
            velocityVector = aimVector;
        }
        velocityVector += randomizedVector;
        
        if(velocityVector.magnitude < 1)
        {
            velocityVector = Vector2.zero;
        }

        velocityVector.Normalize();

        RigidBody.velocity = velocityVector * enemyWalkSpeedModifier;

    }

    private void Aim()
    {
        


        if(Weapon)
            aimVector = targetPosition - (Vector2) Weapon.transform.position;
        else
            aimVector = targetPosition - (Vector2) this.transform.position;

        if((aimVector.x < 0 && flipRight) || (aimVector.x >= 0 && !flipRight))
        {
            UtilityScripts.FlipObject(this.transform, !flipRight);
            flipRight = !flipRight;
        }

        float angle = Mathf.Atan2(aimVector.y, aimVector.x) * Mathf.Rad2Deg;
        Weapon.transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);    //  Reset rotation
        if(flipRight)
            Weapon.transform.Rotate(0.0f, 0.0f, angle);
        else
            Weapon.transform.Rotate(180.0f, 0.0f, -angle);
    }

    internal Vector2 GetAimVector()
    {
        return aimVector;
    }

    private void Randomize()
    {
        randomizedVector = new Vector2(UnityEngine.Random.Range(-0.2f, 0.2f), UnityEngine.Random.Range(-0.2f, 0.2f));
        randomizedVector.Normalize();

    }

    public bool IsFacingRight()
    {
        return flipRight;
    }
}
