﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakWall : MonoBehaviour
{
    public GameObject weak;
    public GameObject brokenWallPrefab;
    public Direction transferDirection;

    public AudioClip audioClip;

    public WeakWall pairedWall;

    private bool isBroken = false;

    public void HeavyHit()
    {
        if(!isBroken)
        {
            BreakWall();
            AudioSource.PlayClipAtPoint(audioClip, transform.position, 0.75f);
            pairedWall.BreakWall();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        HeavyDamageMarker hdm = other.gameObject.GetComponent<HeavyDamageMarker>();
        if (hdm != null) Debug.Log("Heavy damage applied");
        if(other.gameObject.GetComponent<HeavyDamageMarker>() != null && !isBroken)
        {
            BreakWall();
            pairedWall.BreakWall();
        }
        else
        {
            if(other.gameObject.CompareTag("Player") && isBroken)
            {
                switch(transferDirection)
                {
                    case Direction.DIRECTION_LEFT:
                        other.gameObject.transform.position += new Vector3(-1.45f, 0, 0);
                        break;
                    case Direction.DIRECTION_RIGHT:
                        other.gameObject.transform.position += new Vector3(1.45f, 0, 0);
                        break;
                    case Direction.DIRECTION_UP:
                        other.gameObject.transform.position += new Vector3(0, 2.1f, 0);
                        break;
                    case Direction.DIRECTION_DOWN:
                        other.gameObject.transform.position += new Vector3(0, -2f, 0);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void BreakWall()
    {
        Debug.Log("Wall broken!");
        if(!isBroken)
        {
            isBroken = true;
            Instantiate(brokenWallPrefab, weak.transform.position, Quaternion.identity, this.gameObject.transform);
            Destroy(weak);
        }
    }
}
