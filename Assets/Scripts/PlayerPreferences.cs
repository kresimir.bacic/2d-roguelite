﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPreferences : MonoBehaviour
{
    public enum PlayerCharacter
    {
        ARTIFICIAL_INTELLIGENCE,
        CYBORG,
        TOTAL_NUM_OF_CHARACTERS
    };

    public PlayerCharacter selectedCharacter;
}
