﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryScreenManager : MonoBehaviour
{
    public string titleText;
    public string storyText;

    public Text title;
    public Text story;

    private GameManager manager;
    
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        manager.playing = false;

        title.text = titleText;
        story.text = storyText;

        Time.timeScale = 0;
    }

    public void HandleContinuePressed()
    {

        Time.timeScale = 1;
        manager.DestroyScene();

        Jigsaw j = GameObject.FindGameObjectWithTag("Jigsaw").GetComponent<Jigsaw>();
        SceneManager.LoadScene(j.nextLevelName);
    }
}
