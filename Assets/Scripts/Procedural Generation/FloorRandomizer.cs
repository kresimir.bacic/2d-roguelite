﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorRandomizer : MonoBehaviour
{
    private GameObject floorGrid;
    public GameObject[] tilemaps;

    void Start()
    {
        floorGrid = this.GetComponentInChildren<TileGridMarker>().gameObject;
        GameObject defaultTilemap = floorGrid.transform.GetChild(0).gameObject;
        int randomFlooring = Random.Range(0, tilemaps.Length);

        Instantiate(tilemaps[randomFlooring], defaultTilemap.transform.position, Quaternion.identity, floorGrid.transform);
        Destroy(defaultTilemap);
    }
}
