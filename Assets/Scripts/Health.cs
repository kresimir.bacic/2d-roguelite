﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int maxHealth;
    public bool authorized = false;
    public bool isMachine = false;

    public int currentHealth;

    public bool keysHeal = false;

    public System.Action<int, int> OnHealthChanged;
    public System.Action<GameObject> OnHealthDepleted;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public int GetHealth()
    {
        return currentHealth;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public void DealDamage(int i)
    {
        if(i > 0)
        {
            currentHealth -= i;
            if (currentHealth < 0)
            {
                currentHealth = 0;
                if(OnHealthDepleted != null)
                {
                    OnHealthDepleted(this.gameObject);
                }
            }

            if(OnHealthChanged != null)
            {
                OnHealthChanged(currentHealth, maxHealth);
            }
        }

    }

    public void HealHealth(int i)
    {
        if(currentHealth > 0 && i > 0)
        {
            if((currentHealth + i) > maxHealth)
            {
                currentHealth = maxHealth;
            }
            else
            {
                currentHealth += i;
            }

            if(OnHealthChanged != null)
            {
                OnHealthChanged(currentHealth, maxHealth);
            }
        }
    }
}
