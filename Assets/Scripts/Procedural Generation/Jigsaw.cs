﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jigsaw : MonoBehaviour
{
    public GameObject levelScene;

    public GameObject storyCanvas;

    public GameObject[] topClusters;
    public GameObject[] leftClusters;
    public GameObject[] bottomClusters;
    public GameObject[] rightClusters;

    public GameObject tbWeakWallPrefab;
    public GameObject lrWeakWallPrefab;

    public GameObject keyPrefab;

    public string nextLevelName;
    public GameObject bossGroupPrefab;


    public float weakWallSpawnChance = 0.5f;

    private List<GameObject> weakWallSpawnPoints = new List<GameObject>();

    private void Start()
    {
        GameManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        manager.InitScene();

        // Nasumično izabire i instancira clustere za sva vrata centralne sobe
        int rand = Random.Range(0, topClusters.Length);
        Instantiate(topClusters[rand], new Vector3(0, 4, 0), Quaternion.identity, levelScene.GetComponent<Transform>());

        rand = Random.Range(0, leftClusters.Length);
        Instantiate(leftClusters[rand], new Vector3(-4, 0, 0), Quaternion.identity, levelScene.GetComponent<Transform>());

        rand = Random.Range(0, bottomClusters.Length);
        Instantiate(bottomClusters[rand], new Vector3(0, -4, 0), Quaternion.identity, levelScene.GetComponent<Transform>());

        rand = Random.Range(0, rightClusters.Length);
        Instantiate(rightClusters[rand], new Vector3(4, 0, 0), Quaternion.identity, levelScene.GetComponent<Transform>());

        // "Kupi" sve moguće točke pojave slabih zidova i na njima instancira obostrane slabe zidove s nasumičnom šansom
        weakWallSpawnPoints.AddRange(GameObject.FindGameObjectsWithTag("WeakWallSpawnPoint"));
        foreach(GameObject spawn in weakWallSpawnPoints)
        {
            float randF = Random.Range(0, 1);
            if(randF < weakWallSpawnChance)
            {
                // Instancira pripadajući prefab za "gore-dolje" ili "lijevo-desno" pozicionirane pukotine
                Direction d = spawn.GetComponent<WallDirectionMarker>().dir;
                if(d == Direction.DIRECTION_UP || d == Direction.DIRECTION_DOWN)
                {
                    Instantiate(tbWeakWallPrefab, spawn.transform.position, Quaternion.identity);
                }
                else if(d == Direction.DIRECTION_LEFT || d == Direction.DIRECTION_RIGHT)
                {
                    Instantiate(lrWeakWallPrefab, spawn.transform.position, Quaternion.identity);
                }
            }
        }

        StartCoroutine("GenerateBoss");

        manager.playing = true;
        Time.timeScale = 1;
    }

    // Instancira ključ na nasumičnoj lokaciji ovisno o traženoj dohvatljivosti
    public void SpawnKey(bool reachable)
    {
        List<GameObject> adequateKeySpawns = new List<GameObject>();

        // Za svaki spawn point ključa
        foreach(GameObject spawn in GameObject.FindGameObjectsWithTag("KeySpawn"))
        {
            // Ako nije već nešto spawnao
            if (spawn.transform.childCount == 0)
            {
                if(reachable)
                {
                    // Ako treba biti inicijalno dohvatljiv i nalazi se u predvorju clustera, adekvatan je
                    if(spawn.GetComponentInParent<ClusterAtriumMarker>() != null)
                    {
                        adequateKeySpawns.Add(spawn);
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    // Ako ne mora biti inicijalno dohvatljiv, bilo koji prazni spawn je iskoristiv
                    adequateKeySpawns.Add(spawn);
                }
            }
        }

        if(adequateKeySpawns.Count > 0)
        {
            // Uzmi nasumičnu adekvatnu spawn točku
            int rand = Random.Range(0, adequateKeySpawns.Count);

            // Instanciraj ključ na jednoj adekvatnoj točki
            Key k = Instantiate(keyPrefab, adequateKeySpawns[rand].transform.position, Quaternion.identity, adequateKeySpawns[rand].transform).GetComponent<Key>();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().TrackKey(k);
        }
    }

    IEnumerator GenerateBoss()
    {
        yield return new WaitForSecondsRealtime(1);

        DeadEndMarker[] bossCandidates = levelScene.GetComponentsInChildren<DeadEndMarker>();
        int rand = Random.Range(0, bossCandidates.Length);
        RoomController bossRoom = bossCandidates[rand].gameObject.GetComponent<RoomController>();

        bossRoom.DesignateBossRoom(bossGroupPrefab);
    }
}
