﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
public class AILittleRobot : MonoBehaviour
{
    
    public Rigidbody2D robotRigidbody2D;
    public Animator playerAnimator = null;
    public GameObject weapon = null;
    public Animator weaponAnimator = null;
    public GameObject firingPoint;

    public AudioClip shootClip;
    public float shootVolume = 1.0f;

    //private bool facingRight = true;

    private Transform target;
    private Vector2 myTransform;

    private float distance;
    public float distanceToKeep = 0;
    public float moveSpeed = 1.0f;

    public float attackTime = 1.0f;
    public float coolDown = 2.0f;

    public int numberOfProjectiles = 4;
    public float projectileDistance = 50f;

    public GameObject projectile;


    void Awake()
    {
        myTransform = transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        target = go.transform;
        distance = Vector2.Distance(target.position, transform.position);
        if (!robotRigidbody2D) robotRigidbody2D = this.GetComponent<Rigidbody2D>();
        if (!playerAnimator) playerAnimator = this.GetComponent<Animator>();
        if (!weaponAnimator) weaponAnimator = weapon.GetComponent<Animator>();

        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        target = go.transform;

        distance = Vector2.Distance(target.position, transform.position);

        Vector2 direction = target.position - transform.position;
        direction.Normalize();

        /*
        if(direction.x >= 0 && !facingRight)
        {
            SpriteRenderer[] sr = this.GetComponentsInChildren<SpriteRenderer>();
            foreach(SpriteRenderer s in sr)
            {
                s.flipX = facingRight;
            }
            facingRight = true;
        }
        else if(direction.x < 0 && facingRight)
        {
            SpriteRenderer[] sr = this.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer s in sr)
            {
                s.flipX = facingRight;
            }
            facingRight = false;
        }
        */
        //move to target
        if (distance >= distanceToKeep)
        {
            //transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            if(moveSpeed != 0.0)
            {
                robotRigidbody2D.velocity = (direction.normalized * moveSpeed);
            }
        }
        else if (distance < distanceToKeep)
        {
            //transform.position = Vector2.MoveTowards(transform.position, target.position, -1 * moveSpeed * Time.deltaTime);
            if (moveSpeed != 0.0)
            {
                robotRigidbody2D.AddForce(direction * moveSpeed * (-1));
            }
        }

        if (attackTime > 0)
        {
            attackTime -= Time.deltaTime;
            if (attackTime <= 0)
            {
                this.Attack();
                attackTime = coolDown;
            }
        }
        else if(attackTime <= 0)
        {
            this.Attack();
            attackTime = coolDown;
        } 

    }

    private void Attack()
    {
        weaponAnimator.SetBool("Shooting", true);

        float angleStep = 360f / numberOfProjectiles;
        float angle = 0f;

        for (int i = 0; i < numberOfProjectiles; i++)
        {
            float projectileDirX = myTransform.x + Mathf.Sin((angle * Mathf.PI) / 180) * projectileDistance;
            float projectileDirY = myTransform.y + Mathf.Cos((angle * Mathf.PI) / 180) * projectileDistance;

            Vector2 projectileVector = new Vector2(projectileDirX, projectileDirY);
            Vector2 projectileMoveVector = (projectileVector - myTransform).normalized;

            var proj = Instantiate(projectile, firingPoint.transform.position, Quaternion.Euler(0f, 0f, angle));
            
            proj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileMoveVector.x, projectileMoveVector.y);

            Destroy(proj, projectileDistance);
            

            angle += angleStep;
        }

        weaponAnimator.SetBool("Shooting", false);
    }

}
