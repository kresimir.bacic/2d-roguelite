﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterAtriumMarker : MonoBehaviour
{
    //  A purely marker component class. A "cluster atrium room" is the first room of the cluster and is always accessible.
}
