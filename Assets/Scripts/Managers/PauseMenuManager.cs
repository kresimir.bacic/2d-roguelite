﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour
{
    private GameManager manager;

    private void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    public void HandleOnMainMenuPressed()
    {
        manager.DestroyScene();
        Time.timeScale = 1;
        manager.playing = false;
        SceneManager.LoadScene("MenuScene");
    }

    public void HandleOnResumePressed()
    {
        manager.UnpauseGame();
    }
}
