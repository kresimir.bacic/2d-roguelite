﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRandomizer : MonoBehaviour
{
    public int setSize;
    public Sprite[] wallSprites;
    //public double[] spriteChance;
    private GameObject frontWalls;

    void Start()
    {
        frontWalls = this.GetComponentInChildren<FrontWallsMarker>().gameObject;
        SpriteRenderer[] wall = frontWalls.GetComponentsInChildren<SpriteRenderer>();
        int noOfSets = wallSprites.Length / setSize;
        int setIndex = Random.Range(0, noOfSets);
        foreach(SpriteRenderer w in wall)
        {
            int randomSprite = Random.Range(-7, setSize);
            if (randomSprite >= 0) w.sprite = wallSprites[randomSprite + setIndex*setSize];
            else w.sprite = wallSprites[setIndex*setSize];
        }
    }
}
